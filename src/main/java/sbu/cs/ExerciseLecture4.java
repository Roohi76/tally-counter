package sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n)
    {
        long k = 1;
        for (int i = n; i > 0; i--)
        {
            k *= i;
        }
        return k;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n)
    {
        if (n == 1 || n == 2)
        {
            return 1;
        }
        else
        {
            long[] fib = new long[n];
            fib[0] = 1;
            fib[1] = 1;
            for (int i = 2; i < n; i++)
            {
                fib[i] = fib[i-1] + fib[i-2];
            }
            return fib[n-1];
        }
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word)
    {
        int n = word.length();
        StringBuilder drow = new StringBuilder();
        for (int i = n - 1; i >= 0; i--)
        {
            drow.append(word.charAt(i));
        }
        return drow.toString();
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line)
    {
        line = line.replaceAll(" ","");
        line = line.toLowerCase();
        int n = line.length();
        StringBuilder enil = new StringBuilder();
        for (int i = n - 1; i >= 0; i--)
        {
            enil.append(line.charAt(i));
        }
        return enil.toString().equals(line);
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2)
    {
        char[][] dot = new char[str1.length()][str2.length()];
        for (int i = 0; i < str1.length(); i++)
        {
            for (int j = 0; j < str2.length(); j++)
            {
                if (str1.charAt(i) == str2.charAt(j))
                {
                    dot[i][j] = '*';
                }
                else
                {
                    dot[i][j] = ' ';
                }
            }
        }
        return dot;
    }
}
