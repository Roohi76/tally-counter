package sbu.cs;

import java.util.Random;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public String weakPassword(int length)
    {
        String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
        Random random = new Random();
        StringBuilder password = new StringBuilder();
        for (int i = 0; i < length; i++)
        {
            password.append(lowerCaseLetters.charAt(random.nextInt(lowerCaseLetters.length())));
        }
        return password.toString();
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public String strongPassword(int length) throws Exception
    {
        if (length < 3)
        {
            throw new IllegalValueException();
        }
        char[] password = new char[length];
        builder_tester(password);
        return String.valueOf(password);
    }

    public void builder_tester(char[] password)
    {
        String lowerCaseLetters = "abcdefghijklmnopqrstuvwxyz";
        String specialCharacters = " !#$%&'()*+,-./:;<=>?@[]^_`{|}~";
        String numbers = "1234567890";
        String combinedCharacters = lowerCaseLetters + specialCharacters + numbers;
        Random random = new Random();
        for (int i = 0; i < password.length; i++)
        {
            password[i] = combinedCharacters.charAt(random.nextInt(combinedCharacters.length()));
        }
        boolean alpha = false, digit = false, special = false;
        for (char c : password) {
            if (Character.isDigit(c)) {
                digit = true;
            } else if (lowerCaseLetters.indexOf(Character.toLowerCase(c)) != -1) {
                alpha = true;
            } else {
                special = true;
            }
        }
        if (!digit || !alpha || !special)
        {
            builder_tester(password);
        }
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public boolean isFiboBin(int n)
    {
        int[] fibo = new int[n];
        fibo[0] = 1;
        fibo[1] = 1;
        for (int i = 2; i < n; i++)
        {
            fibo[i] = fibo[i-1] + fibo[i-2];
        }
        int[] fiboBin = createFiboBin(fibo);
        for (int i = 0; i < n; i++)
        {
            if (fibo[i] + fiboBin[i] == n)
            {
                return true;
            }
        }
        return false;
    }

    public int[] createFiboBin(int[] fibo)
    {
        int[] fiboTemp = new int [fibo.length];
        System.arraycopy(fibo, 0, fiboTemp, 0, fibo.length);
        int[] fiboBin = new int[fibo.length];
        for (int i = 0; i < fibo.length; i++)
        {
            while (fiboTemp[i] > 0)
            {
                fiboBin[i] += fiboTemp[i] % 2;
                fiboTemp[i] /= 2;
            }
        }
        return fiboBin;
    }
}
